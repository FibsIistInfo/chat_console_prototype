#include <iostream>
#include <stdio.h>
#include <conio.h>
#include <string.h>

#define _WIN32_WINNT 0x501
#include <winsock2.h>

#define MAX_PACKET_SIZE 1024
#define CONNECTION_PORT 8583
#define MAX_CONN 10

#pragma comment(lib,"ws2_32.lib") //Winsock Library

char message[MAX_PACKET_SIZE];
int offset = 0;

using namespace std;

// ���������� �� ������ � WinSock ���� ���: http://citforum.ru/book/cook/winsock.shtml
int main(int argc, char* argv[])
{
   WSADATA ws;
   SOCKET s, new_conn;
   sockaddr_in adr, sock_bind, new_ca;
   hostent* hn;
   char buff [MAX_PACKET_SIZE];

   system("chcp 1251 > nul");
   setlocale(LC_ALL, "Russian");

   if(argc == 2 && argv[1][0] == 'r')
   {
   		printf("\nReciving message: \n");
		// Init
		if (WSAStartup (0x0101, &ws) != 0)
		{
			printf("�� ������� ���������������� �����");
			// Error
			return 2;
		}
		// ������ �����
		if (INVALID_SOCKET == (s = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP) ) )
		{
			printf("\nError: �� ������� ������� �����");
			// Error
			return 3;
		}

		// ��������� ��������
		ZeroMemory (&sock_bind, sizeof (sock_bind));
		sock_bind.sin_family			= PF_INET;
		sock_bind.sin_addr.S_un.S_addr	= htonl (INADDR_ANY);		// ����� ����� ����������� �� "������ ���������" :)
		// ������ ������� ����� ���������������
		sock_bind.sin_port				= htons (CONNECTION_PORT);				// ���� CONNECTION_PORT
		if (SOCKET_ERROR == bind (s, (sockaddr* ) &sock_bind, sizeof (sock_bind) ) )
		{
			printf("\nError: �� ������� bind");
			return 4;
		}

		// ������������� "���������" ����� ��� ������
		if (FAILED (listen (s, MAX_CONN) ) )
		{
			printf("\nError: �� ������� listen");
			return 5;
		}

		printf ("\nWait for incoming connections...\n");

		int i = 0;
		int new_len = 0;
		bool done = false;
		while (!done)
		{
			ZeroMemory (&new_ca, sizeof (new_ca));
			new_len = sizeof (new_ca);
			if (SOCKET_ERROR == (new_conn = accept (s, (sockaddr* ) &new_ca, &new_len) ) )
			{
				// Error
				printf ("\n Accept failed on %d", i);
				return E_FAIL;
			}
			else
			{
				// ������� ���������� � �������
				printf ("\nNew incomming connection %d", i);
				printf ("\nClient IP %s, Client Port %d\n", inet_ntoa ((in_addr) new_ca.sin_addr), ntohs (new_ca.sin_port) );

				// �������� ��������� �� �������
				int msg_len;
				ZeroMemory (&buff, sizeof(buff));
				if (SOCKET_ERROR ==  (msg_len = recv (new_conn, (char* ) &buff, MAX_PACKET_SIZE, 0) ) )
				{
					printf("\nError: �� ������� ��������");
					return 5;
				}

				// ������� ���������.
				for (int c = 0; c<strlen(buff); c++)
					printf ("%c", buff [c]);

				if(strcmp(buff, "/end ") == 0)  // ������ ���������� ������� ��������� �� ����
				{
					done = true; // ��������� ����
				}

				// �������� �����
				sprintf(message, "/recived %d",strlen(buff));
				if (SOCKET_ERROR == send (new_conn, message, sizeof (message), 0) )
				{
					printf("\nError: �� ������� ������� �����");
					// Error
					return 6;
				}

				// ��������� ���������� (���� ����� ��� ������� :) )
				closesocket (new_conn);
			}
			i++;
		}

		// ��������� ����������
		if (SOCKET_ERROR == closesocket (s) )
		{
			printf("\nError: �� ������� ������� ����������");
			// Error
			return 8;
		}
		printf("\nDONE");
   }
   else if(argc >= 3 && argv[1][0] == 's')
   {
		// �������� ���������
		for(int i=3; i<argc && offset<MAX_PACKET_SIZE; i++)
		{
			strncpy(message+offset, argv[i], strlen(argv[i]));
			offset+=strlen(argv[i]);
			strcpy(message+offset, " ");
			offset++;
		}
		printf("\nSending message: %s\n",message);

		// Init
		if (WSAStartup (0x0101, &ws) != 0)
		{
			printf("\nError: �� ������� ���������������� �����");
			// Error
			return 2;
		}
		// ������ �����
		if (INVALID_SOCKET == (s = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP) ) )
		{
			printf("\nError: �� ������� ������� �����");
			// Error
			return 3;
		}
		// ��������� ��������� � �������
		adr.sin_family = AF_INET;
		//adr.sin_addr.S_un.S_addr = *(DWORD* ) hn->h_addr_list[0];
		adr.sin_addr.S_un.S_addr = inet_addr (argv[2]);
		adr.sin_port = htons (CONNECTION_PORT);

		// ������������� ���������� � ��������
		if (SOCKET_ERROR == connect (s, (sockaddr* )&adr, sizeof (adr) ) )
		{
			printf("\nError: �� ������� ���������� ���������� � ��������");
			// Error
			return 5;
		}

		// �������� ������
		if (SOCKET_ERROR == send (s, message, sizeof (message), 0) )
		{
			printf("\nError: �� ������� ������� ������");
			// Error
			return 6;
		}

		// �������� �����
		int len = recv (s, (char *) &buff, MAX_PACKET_SIZE, 0);

		if ( (len == SOCKET_ERROR) || (len == 0) )
		{
			printf("\nError: �� ������� �������� �����");
			// Error
			return 7;
		}

		// ������� ���������
		printf("\nAnswer: \n");
		for (int i = 0; i<strlen(buff); i++)
			printf ("%c", buff [i]);

		printf("\n\n");

		// ��������� ����������
		if (SOCKET_ERROR == closesocket (s) )
		{
			printf("\nError: �� ������� ������� ����������");
			// Error
			return 8;
		}
   }
   else
   {
		printf("\nUsage: \n\"%s  r\" - recive message",argv[0]);
		printf("\n\"%s s ip some_message\" - send some message", argv[0]);
		printf("\n\nExample: \"%s s 127.0.0.1 some_message\" - send some message",argv[0]);
		return 0;
   }
   // ��! :)
   return 1;
}
